﻿using Microsoft.AspNetCore.Mvc;
using SimulationTwitch.Web.UI.Models;

namespace SimulationTwitch.Web.UI.Controllers
{
    public class StreamTwitchController : Controller
    {
        private readonly DefaultDbContext context;
        


        public StreamTwitchController(DefaultDbContext context)
        {
            this.context = context;
            
        }

        

        public IActionResult Index()
        {
           var query = from item in this.context.StreamsTwitch
                        select item;

            return View(query.ToList());
        }

        public IActionResult NouveauStream()
        {
            return this.View();
        }

        [HttpPost]
       
        public IActionResult NouveauStream(StreamAddViewModel item)
        {
            item.UnStream.Status = item.Status;

            this.context.StreamsTwitch.Add(item.UnStream);

            this.context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
