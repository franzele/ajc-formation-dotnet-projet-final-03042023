﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimulationTwitch.Web.UI.Models
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options) { }

        protected DefaultDbContext() { }


        // mettre liaison Classe de models avec table base de données
        public DbSet<StreamTwitchModel> StreamsTwitch { get; set; }
        
    }
}
