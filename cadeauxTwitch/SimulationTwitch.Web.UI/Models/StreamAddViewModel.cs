﻿using cadeau.api;

namespace SimulationTwitch.Web.UI.Models
{
    public class StreamAddViewModel
    {
        public StreamTwitchModel UnStream { get; set; }
        public int Status { get; set; }
    }
}
