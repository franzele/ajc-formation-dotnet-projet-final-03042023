﻿namespace cadeau.api.Adapters
{
    public interface IListeViewer
    {
        List<TwitchViewer> ListeToutViewer { get; set; }

        void AjoutPersonne(TwitchViewer personne, Streamer streamer);
        void VerificationPersonneEligible(Streamer streamer);
    }
}