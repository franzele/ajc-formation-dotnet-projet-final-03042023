﻿namespace cadeau.api.Adapters
{
    public interface IPanierCadeau
    {
        List<Cadeau> Panier { get; set; }

        void AjoutCadeau(Cadeau nouveauCadeau);
    }
}