﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    /// <summary>
    /// Enum TypeCadeau
    /// Tous les types de cadeaux disponibles seront dans cet enumérateur
    /// </summary>
	public enum TypeCadeau
	{
		Reduction,
		Offert,
		TempsAvecStreamer,
		FormationJeu
    }


    /// <summary>
    /// Classe Cadeau
    /// Définition d'un cadeau
    /// Un cadeau possède : un nom, une description et un type (cité dans l'énumérateur)
    /// </summary>
    public class Cadeau

    {
        #region Fields
        private string nom;
		private TypeCadeau type;
		private string description;
        #endregion

        #region Constructor
        public Cadeau() { }
        public Cadeau(string nom, TypeCadeau type)
        {
            this.nom = nom;
            this.type = type;
        }
        public Cadeau(string nom, TypeCadeau type, string description)
        {
            this.nom = nom;
			this.type = type;
			this.description = description;
        }
        #endregion

        #region Properties
        public string Nom { get => nom; set => nom = value; }
        public TypeCadeau Type { get => type; set => type = value; }
        public string Description { get => description; set => description = value; }
        #endregion
    }
}
