﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    /// <summary>
    /// Classe GestionCadeaux
    /// Permets d'effectuer le tirage au sort
    /// </summary>
    public class GestionCadeaux
    {
        #region Fields
        private ListeViewer personnesFideles;
        private PanierCadeau listeCadeaux;
        private Random r = new Random();
        #endregion

        #region Methods
        /// <summary>
        /// Permets de filtrer une liste de viewers, ne laissant que les viewers élliglibes aux cadeaux
        /// </summary>
        /// <param name="listeViewer">
        /// Liste de tous les viewers
        /// </param>
        /// <returns>
        /// Liste de seulement les viewers élligibles aux cadeaux
        /// </returns>
        private List<TwitchViewer> FiltrePersonnesEligibles(ListeViewer listeViewer)
        {
            return listeViewer.ListeToutViewer.Where(c => c.EligibleCadeau).ToList();
        } 

        /// <summary>
        /// Va choisir un viewer élligible aléatoirement et lui attribuer un cadeau aléatoire
        /// </summary>
        /// <param name="nombreTirages">
        /// Nombre de tirage au sort à faire (de base = 1)
        /// </param>
        public void TirerAuSort(int nombreTirages=1)
        {
            var PersonneElligibleCadeau = this.FiltrePersonnesEligibles(personnesFideles);

            for (int i = 0; i < nombreTirages; i++)
            {
                if (PersonneElligibleCadeau.Count > 0 && this.listeCadeaux.Panier.Count > 0)
                {
                    TwitchViewer gagnant = PersonneElligibleCadeau[r.Next(0, PersonneElligibleCadeau.Count)];
                    Cadeau gagné = this.listeCadeaux.Panier[r.Next(0, this.listeCadeaux.Panier.Count)];

                    gagnant.AjouterCadeau(gagné);
                }
            }
        }
		


		#endregion

		#region Constructor
		public GestionCadeaux(List<Personne> listPersonne, List<Cadeau> panierCadeau) { }
        public GestionCadeaux(ListeViewer personneFidele, PanierCadeau listCadeau)
        {
            this.PersonnesFideles = personneFidele;
            this.ListeCadeaux = listCadeau;
        }
        #endregion

        #region Properties
        public ListeViewer PersonnesFideles { get => personnesFideles; set => personnesFideles = value; }
        public PanierCadeau ListeCadeaux { get => listeCadeaux; set => listeCadeaux = value; }
        #endregion
    }
}
