﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cadeau.api.Adapters;

namespace cadeau.api
{
    /// <summary>
    /// Classe ListeViewer
    /// Va lister tous les viewers
    /// </summary>
    public class ListeViewer : IListeViewer
    {
        #region Fields
        private List<TwitchViewer> listeToutViewer = new List<TwitchViewer>();
        private const int pourcentageMin = 70;
        #endregion


        #region Methods
        /// <summary>
        /// Permets d'ajouter un viewer dans la liste des viewers
        /// </summary>
        /// <param name="personne">
        /// Viewer à ajouter dans la liste
        /// </param>
        /// <param name="débutStream"></param>
        public void AjoutPersonne(TwitchViewer personne, Streamer streamer)

        {
            personne.EligibleCadeau = this.PersonneEtreFidele(personne, streamer);

            this.ListeToutViewer.Add(personne);
        }

        /// <summary>
        /// Va mettre à jour la liste, en regardant tous les viewers, et les rends élligibles si c'est le cas
        /// </summary>
        /// <param name="streamer">
        /// Permets de savoir combien de streams ont été fait
        /// </param>
        public void VerificationPersonneEligible(Streamer streamer)
        {
            foreach (var personne in this.ListeToutViewer)
            {
                personne.EligibleCadeau = this.PersonneEtreFidele(personne, streamer);
            }
        }

        /// <summary>
        /// Permets de vérifier si un viewer est élligible pour un cadeau
        /// </summary>
        /// <param name="personne">
        /// Personne, à laquelle le porgramme va vérifier si il est élligible
        /// </param>
        /// <param name="streamer">
        /// Streamer contenant le nombre de stream total
        /// </param>
        /// <returns>
        /// Renvoie True si le viewer si il est élligible, sinon False
        /// </returns>
        private bool PersonneEtreFidele(TwitchViewer personne, Streamer streamer)
        {
            var pourcentageStreamsPasse = personne.NombreDeStream * 100 / (streamer.NombreDeStreams + 1);

            if (pourcentageStreamsPasse > pourcentageMin)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Constructor
        public ListeViewer() { }
        public ListeViewer(List<TwitchViewer> listeToutViewer)
        {
            this.ListeToutViewer = listeToutViewer;
        }
        #endregion

        #region Properties
        public List<TwitchViewer> ListeToutViewer { get => listeToutViewer; set => listeToutViewer = value; }
        #endregion
    }
}
