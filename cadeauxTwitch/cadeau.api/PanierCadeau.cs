﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cadeau.api.Adapters;

namespace cadeau.api
{
    /// <summary>
    /// Classe PanierCadeau
    /// Liste des cadeaux disponibles
    /// </summary>
    public class PanierCadeau : IPanierCadeau
    {
        #region Fields
        private List<Cadeau> panier = new List<Cadeau>();
        #endregion

        #region Methods
        /// <summary>
        /// Permets d'ajouter un cadeau dans la liste de cadeaux
        /// </summary>
        /// <param name="nouveauCadeau">
        /// Cadeau à ajouter dans la liste de cadeaux
        /// </param>
        public void AjoutCadeau(Cadeau nouveauCadeau)
        {
            this.panier.Add(nouveauCadeau);
        }
        #endregion

        #region Constructor
        public PanierCadeau() { }
        #endregion

        #region Properties
        public List<Cadeau> Panier { get => panier; set => panier = value; }
        #endregion
    }
}
