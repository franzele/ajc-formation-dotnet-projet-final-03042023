﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    public abstract class Personne
    {
        #region Fields
        private string prenom;
        private string surnom;
        private string photoDeProfil;
        private string email;
        private bool estConnecte;
        #endregion

        #region Constructor
        public Personne() { }
        public Personne(string surnom, string email, bool connecte)
        {
            Surnom = surnom;
            Email = email;
            EstConnecte = connecte;
        }
        public Personne(string prenom, string surnom, string photoDeProfil, string email, bool estConnecte) 
        {
            Prenom = prenom;
            Surnom = surnom;
            PhotoDeProfil = photoDeProfil;
            Email = email;
            EstConnecte = estConnecte;
        }
        #endregion

        #region Properties
        public string Prenom { get => prenom; set => prenom = value; }
        public string Email { get => email; set => email = value; }
        public bool EstConnecte { get => estConnecte; set => estConnecte = value; }
        public string Surnom { get => surnom; set => surnom = value; }
        public string PhotoDeProfil { get => photoDeProfil; set => photoDeProfil = value; }
        #endregion
    }
}
