﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    /// <summary>
    /// Classe StreamTwitch
    /// </summary>
    public class StreamTwitch
    {
		#region Fields
		private DateTime debutStream = DateTime.Now;
        private int status;
        private string titre;
        private Streamer streamerTwitch;
        #endregion

        #region Constructor
        public StreamTwitch() { }
        public StreamTwitch(DateTime debutStream, Streamer streamer)
        {
            this.DebutStream = debutStream;
            this.streamerTwitch = streamer;
        }
		#endregion

		#region Properties
		public int Id { get; set; }
		public DateTime DebutStream { get => debutStream; set => debutStream = value; }
		public int Status { get => status; set => status = value; }
		public string Titre { get => titre; set => titre = value; }
		[NotMapped]
        public Streamer StreamerTwitch { get => streamerTwitch; set => streamerTwitch = value; }
		#endregion
	}
}
