﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    /// <summary>
    /// Classe Streamer
    /// </summary>
    public class Streamer : Personne
    {
        #region Fields
        private int nombreDeStreams = 0;
        #endregion

        #region Constructor
        public Streamer() { }

        public Streamer(string prenom, string surnom, string photoDeProfil, string email, bool estConnecte) :
            base(surnom, email, estConnecte)
        {
            this.Prenom = prenom;
            this.PhotoDeProfil = photoDeProfil;
        }
        #endregion

        #region Properties
        public int NombreDeStreams { get => nombreDeStreams; set => nombreDeStreams = value; }
        #endregion
    }
}
