﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    /// <summary>
    /// Classe TwitchViewer
    /// </summary>
    public class TwitchViewer : Personne
    {
        #region Fields
        private int nombreDeStream;
        private bool eligibleCadeau = false;
        private List<Cadeau> cadeauGagnes = new List<Cadeau>();
        #endregion

        #region Methods
        /// <summary>
        /// Ajoute le cadeau gagné au viewer
        /// </summary>
        /// <param name="nouveauCadeau">
        /// Cadeau à ajouter dans la liste des cadeaux gagnés
        /// </param>
        public void AjouterCadeau(Cadeau nouveauCadeau)
        {
            this.cadeauGagnes.Add(nouveauCadeau);
        }
        #endregion

        #region Constructor
        public TwitchViewer() { }
        public TwitchViewer(string surnom, string email, bool estConnecte, int nombreDeStream) :
                base(surnom, email, estConnecte)
        {
            this.nombreDeStream = nombreDeStream;
        }
        public TwitchViewer(string prenom, string surnom, string photoDeProfil, string email, bool estConnecte, int nombreDeStream) :
            base(prenom, surnom, photoDeProfil, email, estConnecte)
        {
            this.nombreDeStream = nombreDeStream;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public int NombreDeStream { get => nombreDeStream; set => nombreDeStream = value; }
        public bool EligibleCadeau { get => eligibleCadeau; set => eligibleCadeau = value; }
        [NotMapped]
        public List<Cadeau> CadeauList { get => cadeauGagnes; set => cadeauGagnes = value; }
        #endregion
    }
}
