﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cadeau.api;
using Newtonsoft.Json;
using sauvegarde.api.Adapters;

namespace cadeaux.architecture
{
	/// <summary>
	/// Classe EnregistreurStream
	/// Permets de sauvagarder un objet dans un fichier donné
	/// </summary>
    public class EnregistreurStream : ISauvegardeurTwitch
    {
		#region Fields
		private readonly string cheminFichier;
		#endregion

		#region Methods
        /// <summary>
        /// Permets de sauvagarder les données
        /// </summary>
        /// <param name="obj">
		/// Objet à enregistrer
		/// </param>
		public void Sauvegardeur(Object obj)
        {
			string json = JsonConvert.SerializeObject(obj, Formatting.Indented);

			try
			{
				System.IO.File.WriteAllText(cheminFichier, json);
			}
			catch (PathTooLongException ex)
			{

			}
			catch (IOException ex)
			{

			}
		}
        #endregion

        #region Constructor
        public EnregistreurStream(string cheminFichier)
		{
			this.cheminFichier = $"{cheminFichier}.json";
		}
		#endregion
	}
}
