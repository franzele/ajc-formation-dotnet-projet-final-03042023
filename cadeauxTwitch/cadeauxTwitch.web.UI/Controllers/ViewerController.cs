﻿using Microsoft.AspNetCore.Mvc;
using cadeauxTwitch.web.UI.Models;
using System.Linq;
using cadeau.api;
using System.Linq.Expressions;

namespace cadeauxTwitch.web.UI.Controllers
{
    public class ViewerController : Controller
    {
        private readonly DefaultDbContext context;

        public ViewerController(DefaultDbContext context)
        {
            this.context = context;
        }

       public IActionResult List(bool estFiltre, List<TwitchViewer> listeViewer)
       {
            List<TwitchViewer> personnes;
            if(estFiltre)
            {
                personnes = listeViewer;
            } else
            {
                var query = from item in this.context.TwitchViewers
                            select item;
                personnes = query.ToList();
            }
            return View(personnes);
       }
       
        public IActionResult ListFidele()
        {
            var query = from item in this.context.TwitchViewers
                        where item.EligibleCadeau == true
                        select item;

            List<TwitchViewer> listeViewer = query.ToList();
            return this.List(true, listeViewer);
        }

        [HttpPost]
        public IActionResult List(string typeFiltre)
        {
            switch (typeFiltre.ToLower())
            {
                case "tous":
                    return this.List(false, new List<TwitchViewer>());
                case "fidele":
                    return this.ListFidele();     
                default:
                    return this.List(false, new List<TwitchViewer>());
            }
        }
    }
}
