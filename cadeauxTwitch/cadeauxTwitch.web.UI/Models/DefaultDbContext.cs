﻿using Microsoft.EntityFrameworkCore;
using cadeau.api;

namespace cadeauxTwitch.web.UI.Models
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options) { }

        protected DefaultDbContext() { }

        public DbSet<TwitchViewer> TwitchViewers { get; set; }
    }
}
