﻿using cadeau.api;
using menu.api;
using sauvegarde.api;
using cadeaux.architecture;
using sauvegarde.api.Adapters;
using cadeau.api.Adapters;

Console.WriteLine("\n");
Console.WriteLine("{0," + ((Console.WindowWidth / 2) + ("Bienvenue cher streamer dans la page de gestion des cadeaux des abonnés".Length / 2)) + "}\n", "Bienvenue cher streamer dans la page de gestion des cadeaux des abonnés");
Console.WriteLine("\n");

Streamer streamer = new Streamer();
StreamTwitch stream = new StreamTwitch(DateTime.Now, streamer);

List<Cadeau> cadeaux = new List<Cadeau>() {
new Cadeau("Toupie beyblade", TypeCadeau.Offert, "une toupie en skin"),
new Cadeau("Moment privilégié avec ton Streamer préféré", TypeCadeau.TempsAvecStreamer, "un moment que vous allez passer avec votre streamer préféré"),
new Cadeau("Arme Famas", TypeCadeau.Offert, "une arme pour le jeu Call Of Duty"),
new Cadeau("Formation Call Of Duty", TypeCadeau.FormationJeu, "Une formation complète pour bien apprendre à viser") ,
new Cadeau("Promo sur Fortnite", TypeCadeau.Reduction, "50% de réduction sur l'achat d'un skin Fortnite") };

List<TwitchViewer> viewers = new List<TwitchViewer>() {
new TwitchViewer("pierre", "pierrepomme@gmail.com", true, 80),
new TwitchViewer("Alphonse", "alphonseduval@yahoo.com", true, 80),
new TwitchViewer("Jérome", "jejelacanaille@hotmail.com", true, 80),
new TwitchViewer("Albert", "alberto@gmail.com", true, 80),
new TwitchViewer("heloise", "helohello@gmail.com", true, 80),
new TwitchViewer("Maxime", "maxdam@yahoo.fr", true, 80)};

ListeViewer listPersonne = new ListeViewer();
PanierCadeau panierCadeau = new PanierCadeau();

foreach (Cadeau cadeau in cadeaux)
{
    panierCadeau.AjoutCadeau(cadeau);
}

foreach (TwitchViewer person in viewers)
{
    listPersonne.AjoutPersonne(person, streamer);
}

Console.WriteLine("Liste de personnes :");
Console.WriteLine(" ");

foreach (TwitchViewer viewer in viewers)
{
	string cadeau = viewer.CadeauList.FirstOrDefault()?.Nom ?? "Aucun cadeau";
	Console.WriteLine("- {0} ({1}) : {2}", viewer.Surnom, viewer.Email, cadeau);
	
}


MenuPrincipal menu = new MenuPrincipal();

var ajoutPersonne = new AjoutViewer(listPersonne, Console.WriteLine, Console.ReadLine, streamer);
ListerCadeaux listerCadeaux = new ListerCadeaux(panierCadeau, Console.WriteLine);
var ajoutCadeauManuel = new AjoutCadeauManuel(panierCadeau, Console.WriteLine, Console.ReadLine);
var quitterMenu = new Quitter();

menu.AjoutOptionMenu(new OptionMenu(1, "Ajouter une personne", ajoutPersonne));
menu.AjoutOptionMenu(new OptionMenu(2, "lister les cadeaux présents", listerCadeaux));
menu.AjoutOptionMenu(new OptionMenu(3, "Ajouter un cadeau", ajoutCadeauManuel));
menu.AjoutOptionMenu(new OptionMenu(4, "Quitter le menu", quitterMenu));
menu.Afficher(listPersonne, panierCadeau);



string fichierPersonne = "sauvegardePersonne";
string fichierCadeau = "sauvegardeCadeau";
Sauvegardeur sauvegardeur = new Sauvegardeur(new EnregistreurStream(fichierPersonne), new EnregistreurStream(fichierCadeau));
sauvegardeur.Enregistreur(listPersonne, panierCadeau);




