﻿using cadeau.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ObjectiveC;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	/// <summary>
	/// Interface permetttant d'ajouter manuellement un cadeau au panier de cadeau
	/// </summary>
	public class AjoutCadeauManuel : ISousInterface
	{
		#region Fields
		private PanierCadeau cadeaux;
		private Afficher afficherDelegate;
		private LectureSaisie saisieDelegate;
		#endregion


		#region Methods
		/// <summary>
		/// Va activer la focntion Ajouter(). Elle est nécessaire, à cause de l'interface
		/// </summary>
		public void Afficher()
		{
			Ajouter();
		}

		/// <summary>
		/// Mise en place de l'interface d'ajout de cadeau
		/// </summary>
		public void Ajouter()
		{
			this.afficherDelegate("Nom du cadeau : ");
			string nom = this.saisieDelegate?.Invoke();

			this.afficherDelegate("Quel type de cadeau ?");
			int chiffreAMontrer = 1;
			foreach (var type1 in Enum.GetValues(typeof(TypeCadeau)))
			{
				this.afficherDelegate($"{chiffreAMontrer}. {type1.ToString()}");
				chiffreAMontrer++;
			}

			int typeInt = int.Parse(this.saisieDelegate?.Invoke());
			TypeCadeau type = (TypeCadeau)(typeInt - 1);

			
			this.afficherDelegate("Description du cadeau : ");
			string description = this.saisieDelegate?.Invoke();

			Cadeau nouveauCadeau = new Cadeau(nom, type, description);
			this.cadeaux.AjoutCadeau(nouveauCadeau);
			this.afficherDelegate("Le cadeau a été ajouté avec succès !");
		}
		#endregion

		#region Constructor
		public AjoutCadeauManuel(PanierCadeau cadeaux, Afficher afficherDelegate, LectureSaisie saisieDelegate)
		{
			this.cadeaux = cadeaux;
			this.afficherDelegate = afficherDelegate;
			this.saisieDelegate = saisieDelegate;
		}
		#endregion

		#region Properties
		public PanierCadeau Cadeaux { get => cadeaux; set => cadeaux = value; }
		public Afficher AfficherDelegate { get => afficherDelegate; set => afficherDelegate = value; }
		public LectureSaisie SaisieDelegate { get => saisieDelegate; set => saisieDelegate = value; }
		#endregion
	}
}