﻿using cadeau.api;
using cadeau.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	/// <summary>
	/// Permets d'ajouter manuellement un vierwer à la liste des viewers
	/// </summary>
    public class AjoutViewer : ISousInterface
    {
		#region Fields
		private ListeViewer viewers;
		private Afficher afficherDelegate;
		private LectureSaisie saisieDelegate;
		private Streamer streamer;
		#endregion



		#region Methods
		/// <summary>
		/// Permets d'activer la fonction Ajouter(), et est obligatoire à cause de l'interface
		/// </summary>
		public void Afficher() 
        {
            Ajouter();
        }

		/// <summary>
		/// Renvoie l'interface qui permets d'ajouter un viewer à la liste de viewer
		/// </summary>
		public void Ajouter()
		{
			this.afficherDelegate("Ajout d'un Viewer");

			this.afficherDelegate("Entrez le surnom: ");
			string surnom = this.saisieDelegate?.Invoke();

			this.afficherDelegate("Entrez l'email: ");
			string email = this.saisieDelegate?.Invoke();

			this.afficherDelegate("La personne est-elle connectée (true/false)?: ");
			bool estConnecte = bool.Parse(this.saisieDelegate?.Invoke());

			this.afficherDelegate("Entrez nombre de streams passé: ");
			int tempsPasse = int.Parse(this.saisieDelegate?.Invoke());

			TwitchViewer nouveauViewer = new TwitchViewer(surnom, email, estConnecte, tempsPasse);

			this.viewers.AjoutPersonne(nouveauViewer, streamer);
			this.afficherDelegate("Viewer ajouté avec succès! Voici la liste mise à jour");
			this.afficherDelegate("");
			foreach (TwitchViewer viewer in viewers.ListeToutViewer)
			{
				string cadeau = viewer.CadeauList.FirstOrDefault()?.Nom ?? "Aucun cadeau";
				Console.WriteLine("- {0} ({1}) : {2}", viewer.Surnom, viewer.Email, cadeau);
			}
			
		}
		#endregion

		#region Constructor
		public AjoutViewer(ListeViewer viewers, Afficher afficherDelegate, LectureSaisie saisieDelegate, Streamer streamer)
		{
			this.viewers = viewers;
			this.afficherDelegate = afficherDelegate;
			this.saisieDelegate = saisieDelegate;
			this.streamer = streamer;

		}
		#endregion

		#region Properties
		public ListeViewer Viewers { get => viewers; set => viewers = value; }
		public Afficher AfficherDelegate { get => afficherDelegate; set => afficherDelegate = value; }
		public LectureSaisie SaisieDelegate { get => saisieDelegate; set => saisieDelegate = value; }
		public Streamer Streamer { get => streamer; set => streamer = value; }
		#endregion
	}
}
