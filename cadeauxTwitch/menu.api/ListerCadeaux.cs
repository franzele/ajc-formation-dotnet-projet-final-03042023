﻿using cadeau.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	public class ListerCadeaux : ISousInterface
    {
		#region Fields
		private PanierCadeau cadeaux;
		private Afficher afficherDelegate;
		#endregion

		#region Methods
		/// <summary>
		/// Permets d'afficher les cadeaux dans le panier cadeau
		/// </summary>
		public void Afficher()
        {
			foreach (Cadeau cadeau in cadeaux.Panier)
			afficherDelegate(cadeau.Nom.ToString());  
		}
		/// <summary>
		/// Pas implémenté, mais obligatoire à cause de l'interface
		/// </summary>
		/// <exception cref="NotImplementedException"></exception>
		public void Ajouter()
		{
			throw new NotImplementedException();
		}
		#endregion

		#region Constructor
		public ListerCadeaux(PanierCadeau cadeaux, Afficher afficherDelegate)
		{
			this.cadeaux = cadeaux;
			this.afficherDelegate = afficherDelegate;
		}
		#endregion

		#region Properties
		public PanierCadeau Cadeaux { get => cadeaux; set => cadeaux = value; }
		public Afficher AfficherDelegate { get => afficherDelegate; set => afficherDelegate = value; }
		#endregion
	}
}
