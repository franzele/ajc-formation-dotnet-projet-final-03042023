﻿using cadeau.api;
using cadeau.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	/// <summary>
	/// Menu principal, qui va appeller les autres sous menus
	/// </summary>
    public class MenuPrincipal
    {
		#region Fields
		List<OptionMenu> listeOptionMenu = new List<OptionMenu>();
		#endregion

		#region Methods
		/// <summary>
		/// Permets d'ajouter un sous menu à la liste de sous menu
		/// </summary>
		/// <param name="optionMenu">
		/// Sous menu à ajouter
		/// </param>
		public void AjoutOptionMenu(OptionMenu optionMenu)
        {
            this.ListeOptionMenu.Add(optionMenu);
        }

		/// <summary>
		/// Permets d'afficher le menu, et la liste de sous menu
		/// </summary>
		public void Afficher(ListeViewer listPersonne, PanierCadeau panierCadeau)
		{
			this.EffectuerTirageAuSort(listPersonne, panierCadeau);
			bool continuer = true;
			while (continuer)
			{
				Console.WriteLine(" ");
				Console.WriteLine("Menu, quel est ton choix ?");
				Console.WriteLine(" ");
				foreach (var option in this.ListeOptionMenu)
				{
					Console.WriteLine($"{option.ChiffreId}. {option.Description}");
				}
				var choixUtilisateur = Console.ReadLine();

				foreach (var option in this.ListeOptionMenu)
				{
					if (choixUtilisateur == option.ChiffreId.ToString())
					{
						option.Classe.Afficher();
						if (option.ChiffreId == 4) // Si l'option choisie est "Quitter"
						{
							continuer = false;
						}
						break; // sortir de la boucle foreach car l'option a été trouvée
					}
				}
			}
			this.EffectuerTirageAuSort(listPersonne, panierCadeau);
		}
		/// <summary>
		/// Fait le tirage au sort
		/// </summary>
		/// <param name="listPersonne">
		/// La liste des viewers
		/// </param>
		/// <param name="panierCadeau">
		/// La liste des cadeaux
		/// </param>
		private void EffectuerTirageAuSort(ListeViewer listPersonne, PanierCadeau panierCadeau)
		{
			Console.WriteLine();
			Console.WriteLine("Souhaitez-vous effectuer le tirage au sort ? (oui/non)");
			string reponse = Console.ReadLine();

			if (reponse.Equals("oui"))
			{
				GestionCadeaux gestion = new GestionCadeaux(listPersonne, panierCadeau);
				gestion.TirerAuSort(4);

				Console.WriteLine("Tirage au sort effectué !");
				Console.WriteLine("Liste de personnes et leur cadeau après tirage au sort :");
				Console.WriteLine(" ");
				foreach (TwitchViewer viewer in listPersonne.ListeToutViewer)
				{
					string cadeau = viewer.CadeauList.FirstOrDefault()?.Nom ?? "Aucun cadeau";
					Console.WriteLine("- {0} ({1}) : {2}", viewer.Surnom, viewer.Email, cadeau);
				}
			}
			else
			{
				Console.WriteLine("Poursuite du programme...");
			}
		}
		#endregion

		#region Constructor
		public MenuPrincipal() { }
		#endregion

		#region Properties
		public List<OptionMenu> ListeOptionMenu { get => listeOptionMenu; set => listeOptionMenu = value; }
		#endregion
	}
}
