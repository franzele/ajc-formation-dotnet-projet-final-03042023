﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    /// <summary>
    /// Défintion d'un sous menu
    /// </summary>
    public class OptionMenu
    {
		#region Fields
		private int chiffreId;
		private string description;
		private ISousInterface classe;
		#endregion

		#region Constructor
		public OptionMenu(int chiffreId, string description, ISousInterface classe)
        {
            this.ChiffreId = chiffreId;
            this.Description = description;
            this.Classe = classe;
        }
		#endregion

		#region Properties
		public int ChiffreId { get => chiffreId; set => chiffreId = value; }
		public string Description { get => description; set => description = value; }
		public ISousInterface Classe { get => classe; set => classe = value; }
		#endregion
	}
}
