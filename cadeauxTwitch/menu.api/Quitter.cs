﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	/// <summary>
	/// Permets de quitter l'interface du menu
	/// </summary>
	public class Quitter : ISousInterface
	{
		#region Fields
		public bool continuer;
		#endregion

		#region Methods
		/// <summary>
		/// Affiche un message de fin, et quitte le menu
		/// </summary>
		public void Afficher()
		{
			Console.WriteLine("Retour au menu principal");
			Console.WriteLine("");
			// Modifier la valeur de la variable continuer pour sortir de la boucle while
			// et revenir au menu principal
			continuer = false;
		}

		/// <summary>
		/// Méthode non implémentée
		/// </summary>
		/// <exception cref="NotImplementedException"></exception>
		public void Ajouter()
		{
			throw new NotImplementedException();
		}
		#endregion

		#region Properties
		public bool Continuer { get => continuer; set => continuer = value; }
		#endregion
	}
}



