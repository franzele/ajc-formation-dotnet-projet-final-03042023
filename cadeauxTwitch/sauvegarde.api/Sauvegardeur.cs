﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using cadeau.api.Adapters;
using Newtonsoft.Json;
using sauvegarde.api.Adapters;

namespace sauvegarde.api
{
    public class Sauvegardeur
    {
        private readonly ISauvegardeurTwitch enregistreurPersonne;
        private readonly ISauvegardeurTwitch enregistreurCadeau;


        public Sauvegardeur(ISauvegardeurTwitch sauvegardeurPersonne, ISauvegardeurTwitch sauvegardeurCadeau)
        {
            this.enregistreurPersonne = sauvegardeurPersonne;
            this.enregistreurCadeau = sauvegardeurCadeau;
        }

        public void Enregistreur(IListeViewer listePersonne, IPanierCadeau listeCadeau)
        {
            this.enregistreurPersonne.Sauvegardeur(listePersonne);
            this.enregistreurCadeau.Sauvegardeur(listeCadeau);
        }
    }
}

